# Figure 1
## Diagram
```mermaid
flowchart TB
    A[End User]
    B[User X]
    C[User Y]
    D[User Z]
    E[User A]
    F[User B]
    G[User C]
    H[User M]
    I[User N]
    J[User O]
    B <--> C
    C <--> D
    D <--> B
    E <--> F
    F <--> G
    G <--> E
    H <--> I
    I <--> J
    J <--> H
```
## Source code
````
flowchart TB
A[End User]
B[User X]
C[User Y]
D[User Z]
E[User A]
F[User B]
G[User C]
H[User M]
I[User N]
J[User O]
B <--> C
C <--> D
D <--> B
E <--> F
F <--> G
G <--> E
H <--> I
I <--> J
J <--> H
````