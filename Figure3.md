# Figure 3
## Diagram
````mermaid
flowchart TD
    subgraph network
        A[End User]
        B[User X]
        C[User Y]
        D[User Z]
        E[User A]
        F[User B]
        G[User C]
        H[User M]
        I[User N]
        J[User O]
        subgraph subnetwork 1
            A <--> B
            B <--> C
            C <--> D
            C <--> A
            D <--> B
            D <--> A
        end
        subgraph subnetwork 2
            H <--> E
            E <--> J
            G <--> H
            H <--> J
            G <--> J
            G <--> E
        end
        subgraph subnetwork 3
            F <--> I
        end
    end
````
## Source code
````
flowchart TD
    subgraph network
        A[End User]
        B[User X]
        C[User Y]
        D[User Z]
        E[User A]
        F[User B]
        G[User C]
        H[User M]
        I[User N]
        J[User O]
        subgraph subnetwork 1
            A <--> B
            B <--> C
            C <--> D
            C <--> A
            D <--> B
            D <--> A
        end
        subgraph subnetwork 2
            H <--> E
            E <--> J
            G <--> H
            H <--> J
            G <--> J
            G <--> E
        end
        subgraph subnetwork 3
            F <--> I
        end
    end
````