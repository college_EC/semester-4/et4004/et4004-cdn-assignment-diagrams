# Figure 4
## Diagram
````mermaid
flowchart TD
    subgraph globe
        subgraph America
            subgraph North America
                subgraph USA
                    subgraph US east
                        A["New York (US Headquarters)"]
                        B[Virginia]
                        C[Ohio]
                        D[Florida]
                    end
                    subgraph US west
                        E[Los Angeles]
                        F[Colorado]
                        G[New Mexico]
                    end
                end
                subgraph Canada
                    H[Vancouver]
                    I[Nova Scotia]
                end
                subgraph Mexico
                    J[Mexico City]
                end
            end
            subgraph South America
                K[Sao Paolo]
                L[Santiago]
                M[Lima]
            end
            subgraph Carribean
                N[Dominica]
                O[Havana]
            end
        end
        subgraph Eurasia
            subgraph Europe
                subgraph Europe East
                    P[Warsaw]
                    Q[Kiev]
                    R[Athens]
                end
                subgraph Europe West
                    S[Dublin]
                    T[Frankfurt]
                    U[Madrid]
                    user{{"End user 1: Limerick, Ireland"}}
                end
            end
            subgraph Middle East
                V[Riyadh]
                W[Quatar]
                X[Tel Aviv]
            end
            subgraph Russia
                Y[Moscow]
                Z[Vladivostok]
                AA[Sevastapol]
                AB[Novosibirsk]
            end
            subgraph China
                AC((Beijing))
                AD((Wuhan))
                AE((Wulumuqi))
            end
            subgraph South East
                subgraph Japan
                    AF[Tokyo]
                    AG[Hiroshima]
                end
                subgraph Taiwan
                    AH[Taiwan]
                end
                subgraph Indonesia
                    AI[Jakarta]
                    AJ[Batam]
                    AK[Yogyakarta]
                end
            end
        end
        subgraph Africa
            AP[Cairo]
            AQ[Cape Town]
            AR[Nairobi]
            AS[Dakar]
        end
        subgraph Oceania
            AL[Sydney]
            AM[Melbourne]
            AN[Brisbane]
            AO[Aukland]
        end
    end
    user <--> S
````

## Source code
````
flowchart TD
    subgraph globe
        subgraph America
            subgraph North America
                subgraph USA
                    subgraph US east
                        A["New York (US Headquarters)"]
                        B[Virginia]
                        C[Ohio]
                        D[Florida]
                    end
                    subgraph US west
                        E[Los Angeles]
                        F[Colorado]
                        G[New Mexico]
                    end
                end
                subgraph Canada
                    H[Vancouver]
                    I[Nova Scotia]
                end
                subgraph Mexico
                    J[Mexico City]
                end
            end
            subgraph South America
                K[Sao Paolo]
                L[Santiago]
                M[Lima]
            end
            subgraph Carribean
                N[Dominica]
                O[Havana]
            end
        end
        subgraph Eurasia
            subgraph Europe
                subgraph Europe East
                    P[Warsaw]
                    Q[Kiev]
                    R[Athens]
                end
                subgraph Europe West
                    S[Dublin]
                    T[Frankfurt]
                    U[Madrid]
                    user{{"End user 1: Limerick, Ireland"}}
                end
            end
            subgraph Middle East
                V[Riyadh]
                W[Quatar]
                X[Tel Aviv]
            end
            subgraph Russia
                Y[Moscow]
                Z[Vladivostok]
                AA[Sevastapol]
                AB[Novosibirsk]
            end
            subgraph China
                AC((Beijing))
                AD((Wuhan))
                AE((Wulumuqi))
            end
            subgraph South East
                subgraph Japan
                    AF[Tokyo]
                    AG[Hiroshima]
                end
                subgraph Taiwan
                    AH[Taiwan]
                end
                subgraph Indonesia
                    AI[Jakarta]
                    AJ[Batam]
                    AK[Yogyakarta]
                end
            end
        end
        subgraph Africa
            AP[Cairo]
            AQ[Cape Town]
            AR[Nairobi]
            AS[Dakar]
        end
        subgraph Oceania
            AL[Sydney]
            AM[Melbourne]
            AN[Brisbane]
            AO[Aukland]
        end
    end
    user <--> S
````