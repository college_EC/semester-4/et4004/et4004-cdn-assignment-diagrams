# Figure 6
## Diagram
````mermaid
flowchart LR
    subgraph globe
        subgraph America
            subgraph USA
                subgraph East
                    E[New York]
                end
                subgraph West
                    J[Los Angeles]
                end
            end
            subgraph Canada
                F[Vancouver]
            end
        end
        subgraph Eurasia
            subgraph Europe
                subgraph Eastern Europe
                    I[Warsaw]
                end
                subgraph Western Europe
                    A[Dublin]
                    B[Frankfurt]
                    C[Madrid]
                    U2{UK}
                    U1{Ireland}
                end
            end
            subgraph Asia
                subgraph Middle East
                    G[Qatar]
                    H[Tel Aviv]
                end
            end
            subgraph Oceania
                D[Melbourne]
            end
        end
    end
    U1 ==1000000==> A
    U1 --350000--> B
    U2 --9650000--> B
    U2 --5000000--> C
    U2 --6000000--> I
    U2 --9500000--> E
````
## Code
````mermaid.js
flowchart LR
    subgraph globe
        subgraph America
            subgraph USA
                subgraph East
                    E[New York]
                end
                subgraph West
                    J[Los Angeles]
                end
            end
            subgraph Canada
                F[Vancouver]
            end
        end
        subgraph Eurasia
            subgraph Europe
                subgraph Eastern Europe
                    I[Warsaw]
                end
                subgraph Western Europe
                    A[Dublin]
                    B[Frankfurt]
                    C[Madrid]
                    U2{UK}
                    U1{Ireland}
                end
            end
            subgraph Asia
                subgraph Middle East
                    G[Qatar]
                    H[Tel Aviv]
                end
            end
            subgraph Oceania
                D[Melbourne]
            end
        end
    end
    U1 ==1000000==> A
    U1 --350000--> B
    U2 --9650000--> B
    U2 --5000000--> C
    U2 --6000000--> I
    U2 --9500000--> E
````