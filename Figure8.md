# Figure 8
## Diagram
````mermaid
flowchart TB
    subgraph root
        subgraph html
            page[index.html]
        end
        subgraph resources
            subgraph imgs
                image[logo.png]
            end
        end
    end
````
## Code
````
flowchart TB
    subgraph root
        subgraph html
            page[index.html]
        end
        subgraph resources
            subgraph imgs
                image[logo.png]
            end
        end
    end
````